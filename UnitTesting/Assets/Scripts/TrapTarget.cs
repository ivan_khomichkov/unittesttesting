﻿using UnityEngine;

public class TrapTarget : MonoBehaviour, ITrapTarget
{
    private CharacterController charracterController;
    [SerializeField] private bool isPlayer = false;
    public int Health { get; set; }

    public bool IsPlayer => isPlayer;

    private void Awake()
    {
        charracterController = GetComponent<CharacterController>();
    }

    void Update()
    {
        var vertical = Input.GetAxis("Vertical");
        var horizontal = Input.GetAxis("Horizontal");

        charracterController.Move(new Vector3(horizontal, 0, vertical));
    }
}
