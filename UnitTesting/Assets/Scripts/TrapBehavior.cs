﻿using UnityEngine;

public class TrapBehavior : MonoBehaviour
{
    [SerializeField] private TrapTargetType trapTargetType = TrapTargetType.NPC;

    private TrapLogic trap;

    private void Awake()
    {
        trap = new TrapLogic();
    }

    private void OnTriggerEnter(Collider colider)
    {
        var trapTarget = colider.GetComponent<TrapTarget>();
        trap.HandlePlayerTrapEnter(trapTarget, trapTargetType);
    }
}

public class TrapLogic
{
    public void HandlePlayerTrapEnter(ITrapTarget trapTarget, TrapTargetType trapTargetType)
    {
        if (trapTarget.IsPlayer)
        {
            if (trapTargetType == TrapTargetType.Player)
            {
                trapTarget.Health--;
            }
        }
        else
        {
            if (trapTargetType == TrapTargetType.NPC)
            {
                trapTarget.Health--;
            }
        }

    }
}

public enum TrapTargetType
{
    NPC, Player
}
