﻿public interface ITrapTarget
{
    int Health { get; set; }
    bool IsPlayer { get; }
}