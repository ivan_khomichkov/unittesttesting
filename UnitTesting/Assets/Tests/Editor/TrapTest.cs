﻿using NSubstitute;
using NUnit.Framework;

namespace Tests.Editor
{
    public class TrapTest
    {
        // A Test behaves as an ordinary method
        [Test]
        public void player_got_in_trap_reduce_health()
        {
            var trap = new TrapLogic();

            ITrapTarget trapTarget = Substitute.For<ITrapTarget>();

            trapTarget.IsPlayer.Returns(true);
            trap.HandlePlayerTrapEnter(trapTarget, TrapTargetType.Player);

            Assert.AreEqual(trapTarget.Health, -1);
            Assert.IsInstanceOf(typeof(TrapLogic),trap);

            // Use the Assert class to test conditions
        }
        [Test]
        public void NPC_got_In_trap_reduce_health()
        {
            var trap = new TrapLogic();

            ITrapTarget trapTarget = Substitute.For<ITrapTarget>();
            
            trap.HandlePlayerTrapEnter(trapTarget, TrapTargetType.NPC);

            Assert.AreEqual(trapTarget.Health, -1);
            Assert.IsInstanceOf(typeof(TrapLogic),trap);

        }
       
    }
}
